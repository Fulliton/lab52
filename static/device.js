'use strict'
export default class Device {
	constructor (canvas, ctx) {
		this.canvas = canvas
		this.ctx = ctx
		this.data = {
      currentValue: 0,
      accuracy: 0.25,
      step: 0.01,
			Ng: []
		};

	}
	clear () {
		this.ctx.save();
	  this.ctx.setTransform(1, 0, 0, 1, 0, 0);
	  this.ctx.clearRect(0, 0, canvas.width, canvas.height);
	  this.ctx.restore();
	}
	draw (numberPlates, salineName) {
		this.canvas.width = 400;
		this.canvas.height = 300;
		
		this.ctx.strokeStyle = "black";

		// main
		this.ctx.strokeRect(10, 100, 380, 150);
		this.ctx.strokeRect(130, 80, 140, 20);
    this.drawPlatesAndSaline(numberPlates, salineName);

		// ------------------- sensor
    this.drawSensor("");
		
		// ------------------- buttons
		this.ctx.moveTo(180, 80);
		// for (let i = 0; i < 2; i++) {
		// 	for (let j = 0; j < 2; j++) {
		// 		this.ctx.strokeRect(330 + (i * 30), 120 + (j * 30), 20, 20);
		// 	}
		// }
		//
		// for (let i = 0; i < 2; i++) {
		// 	for (let j = 0; j < 1; j++) {
		// 		this.ctx.strokeRect(330 + (i * 30), 215 + (j * 30), 20, 20);
		// 	}
		// }
		//
		// for (let i = 0; i < 2; i++) {
		// 	for (let j = 0; j < 1; j++) {
		// 		this.ctx.strokeRect(230 + (i * 40), 215 + (j * 40), 30, 20);
		// 	}
		// }
    this.ctx.font = "12pt arial";
    this.ctx.textAlign = "center";
    this.ctx.textBaseline = "middle";

    this.ctx.strokeRect(230, 205, 60, 30);
    this.ctx.fillText("Сброс", 260, 220);

    this.ctx.strokeRect(310, 205, 60, 30);
    this.ctx.fillText("Запуск", 340, 220);
	}
	drawAction (Ng, time, numberPlates, salineName) {
    this.data.step = Ng / (time / 1000) / 10;
    let self = this;
    let intervalID = setInterval(function() {
      self.clear();
      self.draw(numberPlates, salineName);
      self.drawSensor(self.data.currentValue);
      self.data.currentValue += self.data.step;

      if (Ng <= self.data.currentValue) {
        self.data.currentValue = Ng;
        clearInterval(intervalID);
        self.clear();
        self.draw(numberPlates, salineName);
        self.drawSensor(self.data.currentValue);
        self.reset()
      }
    }, 100);
	}
	drawSensor (Ng) {
    this.ctx.font = "22pt arial";
    this.ctx.textAlign = "center";
    this.ctx.textBaseline = "middle";
    Ng = Ng.toString().substr(0, 4);
    this.ctx.strokeRect(25, 150, 125, 85); 
    this.ctx.fillText(Ng, 87, 193);
	}
	setNg (Ng) {
		this.data['Ng'] = Ng;
	}
	drawPlatesAndSaline(count, salineName) {
	  if (count) {
      for (let i = 0; i < count; i++) {
        this.ctx.strokeRect(150, 75 - (5 * i), 100, 5);
      }
    }

    if (salineName != null || salineName != undefined) {
      this.ctx.strokeRect(163, 70 - (5 * count), 76, 10);
      this.ctx.font = "12pt arial";
      this.ctx.textAlign = "center";
      this.ctx.fillText(salineName, 200, 60 - (5 * count));
    }
  }
  reset() {
    this.data.currentValue = 0;
  }
}
